package org.momenttrail.model.table;

import javafx.beans.property.SimpleStringProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class TaskStatsTableRowTest {

    TaskStatsTableRow taskStatsTableRow;

    @BeforeEach
    void setUp() {
        taskStatsTableRow = new TaskStatsTableRow();
        taskStatsTableRow.date.set("2020-05-13");
        taskStatsTableRow.start.set("17:45");
        taskStatsTableRow.end.set("19:15");
        taskStatsTableRow.totalTime.set("15963");
    }

    @Test
    void getDate() {
        assertThat(taskStatsTableRow.getDate()).isEqualTo("2020-05-13");
    }

    @Test
    void getStart() {
        assertThat(taskStatsTableRow.getStart()).isEqualTo("17:45");
    }

    @Test
    void getEnd() {
        assertThat(taskStatsTableRow.getEnd()).isEqualTo("19:15");
    }

    @Test
    void getTotalTime() {
        assertThat(taskStatsTableRow.getTotalTime()).isEqualTo("15963");
    }
}