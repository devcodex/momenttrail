package org.momenttrail.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

class TaskViewControllerTest {

    private TaskViewController taskViewController;

    @Mock
    private TimerController timerController;
    @Mock
    private MainWindowController parentController;
    @Mock
    private ContentControlController contentControlController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        taskViewController = new TaskViewController(contentControlController);
    }

    @Test
    void convertSecondsToTime() {
        // 27915 - 07:45:15
        assertThat(taskViewController.convertSecondsToTime(27915)).isEqualTo("07:45:15");
    }
}