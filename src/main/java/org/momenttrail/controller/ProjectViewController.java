package org.momenttrail.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.controller.popup.CategoryOperationsController;
import org.momenttrail.controller.popup.ProjectOperationsController;
import org.momenttrail.dao.service.ProjectService;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Category;
import org.momenttrail.model.Project;
import org.momenttrail.model.table.ProjectStatsTableRow;

import java.io.IOException;

@Getter @Setter
public class ProjectViewController {
    ContentControlController contentControlController;
    ProjectService projectService;

    /* Properties */
    private Project project;

    /* UI Elements */
    @FXML
    private AnchorPane contentPane;
    @FXML
    private Label titleLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Button buttonAddCategory;
    @FXML
    private Button buttonDelete;
    @FXML
    private Button buttonModify;

    /* Table View Elements */
    @FXML
    private TableView<ProjectStatsTableRow> projectStatsTable;
    @FXML
    private TableColumn<ProjectStatsTableRow, String> categoryColumn = new TableColumn<>();
    @FXML
    private TableColumn<ProjectStatsTableRow, String> numTasksColumn = new TableColumn<>();

    private static final Logger logger = LogManager.getLogger(ProjectViewController.class);

    public ProjectViewController() {
        projectService = ProjectService.getInstance();
    }

    /**
     * Initialises the content of the view
     * The controller itself is initialised in MainWindowController
     * @see org.momenttrail.controller.MainWindowController
     * The contents of this view are instanciated with the following method
     * @see org.momenttrail.controller.MainWindowController#loadProjectContent(Project)
     */
    public void initializeContent() {
        setTitleLabel(project.getTitle());
        setDescriptionLabel(project.getDescription());

        initButtons();
        initListView();
    }

    /**
     * Loads a new window to allow the modification of a given Project
     * @param project - Project object to be modified
     */
    public void loadModifyProjectWindow(Project project) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
            fxmlLoader.setController(new ProjectOperationsController(project, OperationType.MODIFY, contentControlController));
            Stage stage = new Stage();
            stage.setTitle("Modify " + project.getTitle());
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.showAndWait();
        } catch (IOException e) {
            logger.error("Failed to load fxml for project modification.");
        }
    }

    /**
     * Loads a new window to allow the addition of a new category to current project
     */
    public void loadAddCategoryWindow() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
            Category category = new Category();
            category.setProject(this.project);
            fxmlLoader.setController(new CategoryOperationsController(category, OperationType.CREATE, contentControlController));
            Stage stage = new Stage();
            stage.setTitle("Add new category");
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.showAndWait();
        } catch (IOException e) {
            logger.error("Failed to load fxml for adding new category.");
        }
    }

    /**
     * Initialises button actions for this controller
     */
    private void initButtons() {
        buttonAddCategory.setOnAction((actionEvent -> loadAddCategoryWindow()));

        buttonDelete.setOnAction((actionEvent -> contentControlController.onProjectDeleteAction(project)));

        buttonModify.setOnAction((actionEvent -> loadModifyProjectWindow(this.project)));
    }

    /**
     * Initialises ListView of categories for this controller
     */
    private void initListView() {
        categoryColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
        categoryColumn.prefWidthProperty().bind(projectStatsTable.widthProperty().multiply(0.8));
        numTasksColumn.setCellValueFactory(new PropertyValueFactory<>("numTasks"));
        numTasksColumn.prefWidthProperty().bind(projectStatsTable.widthProperty().multiply(0.2));



        ObservableList<ProjectStatsTableRow> projectStatsTableList = FXCollections.observableArrayList();
        for (Category category : this.project.getCategoryList()) {
            ProjectStatsTableRow projectStatsTableRow = new ProjectStatsTableRow();
            projectStatsTableRow.category.set(category.getTitle());
            projectStatsTableRow.numTasks.set(Integer.toString(category.getTaskList().size()));
            projectStatsTableList.add(projectStatsTableRow);
        }

        projectStatsTable.setItems(projectStatsTableList);
    }

    /* Getters and Setters */
    public String getTitleLabel() {
        return titleLabel.getText();
    }

    public void setTitleLabel(String titleLabel) {
        this.titleLabel.setText(titleLabel);
    }

    public String getDescriptionLabel() {
        return descriptionLabel.getText();
    }

    public void setDescriptionLabel(String descriptionLabel) {
        this.descriptionLabel.setText(descriptionLabel);
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
