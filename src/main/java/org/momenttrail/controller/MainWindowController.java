package org.momenttrail.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.controller.popup.ExportController;
import org.momenttrail.model.Category;
import org.momenttrail.model.Project;
import org.momenttrail.model.Task;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {

    // Child controllers
    ContentControlController contentControlController;
    TimerController timerController;
    ProjectViewController projectViewController;
    CategoryViewController categoryViewController;
    TaskViewController taskViewController;

    @FXML
    private AnchorPane contentPane;
    @FXML
    private AnchorPane contentControlPane;
    @FXML
    private AnchorPane timerPane;
    @FXML
    private MenuItem exportMenuItem;
    @FXML
    private MenuItem quitMenuItem;

    private static final Logger logger = LogManager.getLogger(MainWindowController.class);

    public MainWindowController() {
        contentControlController = new ContentControlController();
        contentControlController.setParentController(this);
        timerController = TimerController.getInstance();
        timerController.setParentController(this);
        projectViewController = new ProjectViewController();
        projectViewController.contentControlController = contentControlController;
        categoryViewController = new CategoryViewController();
        categoryViewController.contentControlController = contentControlController;
        taskViewController = new TaskViewController(contentControlController);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadContentControllerPane();
        loadTimerPane();
        initMenuItem();


    }

    /**
     * Loads the ContentControlPane onto the main view
     */
    public void loadContentControllerPane() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("contentControl.fxml"));
            fxmlLoader.setController(contentControlController);
            Region region = fxmlLoader.load();
            contentControlPane.getChildren().add(region);
            region.prefWidthProperty().bind(contentControlPane.widthProperty());
            region.prefHeightProperty().bind(contentControlPane.heightProperty());
        } catch (IOException e) {
            logger.error("Failed to load fxml for content control view.");
        }
    }

    /**
     * Load the TimerPane on to the main view
     */
    public void loadTimerPane() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("content/timerView.fxml"));
            fxmlLoader.setController(timerController);
            Region region = fxmlLoader.load();
            timerPane.getChildren().add(region);
            region.prefWidthProperty().bind(timerPane.widthProperty());
            region.prefHeightProperty().bind(timerPane.heightProperty());
            timerController.setParentController(this);
        } catch (IOException e) {
            logger.error("Failed to load fxml for timer view.");
        }
    }

    /**
     * Loads a given project onto the ContentPane
     * @param project - Project object to be displayed on the view
     */
    @FXML
    public void loadProjectContent(Project project) {
        this.contentPane.getChildren().clear();
        if(project != null) {
            projectViewController.setProject(project);

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("content/projectView.fxml"));
            fxmlLoader.setController(projectViewController);

            loadContentPane(fxmlLoader);
            projectViewController.initializeContent();
        }
    }

    /**
     * Loads a given Category onto the ContentPane
     * @param category - Category object to be displayed on the view
     */
    @FXML
    public void loadCategoryContent(Category category) {
        this.contentPane.getChildren().clear();
        if(category != null) {
            categoryViewController.setCategory(category);

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("content/categoryView.fxml"));
            fxmlLoader.setController(categoryViewController);

            loadContentPane(fxmlLoader);
            categoryViewController.initializeContent();
        }
    }

    /**
     * Loads a given task onto the ContentPane
     * @param task - Task object to be displayed on the view
     */
    @FXML
    public void loadTaskContent(Task task) {
        this.contentPane.getChildren().clear();
        if(task != null) {
            taskViewController.setTask(task);

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("content/taskView.fxml"));
            fxmlLoader.setController(taskViewController);

            loadContentPane(fxmlLoader);
            taskViewController.initializeContent();
        }

    }

    /**
     * Utility method to load views into content pane
     * @param loader - FXML loader to load view into content pane
     */
    public void loadContentPane(FXMLLoader loader) {
        try {
            Region region = loader.load();
            this.contentPane.getChildren().add(region);
            region.prefWidthProperty().bind(contentPane.widthProperty());
            region.prefHeightProperty().bind(contentPane.heightProperty());
        } catch (IOException e) {
            logger.error("Failed to load fxml for selected element content.");
        }
    }

    public void initMenuItem() {
        exportMenuItem.setOnAction((actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/csvExport.fxml"));
                fxmlLoader.setController(new ExportController());
                Stage stage = new Stage();
                stage.setTitle("Export data to csv");
                stage.setScene(new Scene(fxmlLoader.load()));
                stage.show();
            } catch (IOException e) {
                logger.error("Failed to load fxml for export operation window.");
            }
        }));

        quitMenuItem.setOnAction((actionEvent -> {
            Platform.exit();
            System.exit(0);
        }));
    }

    public AnchorPane getContentPane() {
        return contentPane;
    }
}
