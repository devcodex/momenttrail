package org.momenttrail.controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.dao.service.TimeIntervalService;
import org.momenttrail.model.Task;
import org.momenttrail.model.TimeInterval;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.ResourceBundle;

public class TimerController implements Initializable {

    private static TimerController timerController;

    private MainWindowController parentController;
    private Task activeTask;
    private final TimeIntervalService timeIntervalService;
    private boolean timerRunning = false;
    private final Calendar runningTime;
    private final SimpleDateFormat timerFormat = new SimpleDateFormat("HH:mm:ss");
    private Timeline timelineClock;
    private Timeline timelineIntervalSave;
    private TimeInterval currentTimeInterval;
    private int currentSeconds;


    @FXML
    private AnchorPane contentPane;
    @FXML
    private Button timerStartButton;
    @FXML
    private Button timerStopButton;
    @FXML
    private Label activeTaskLabel;
    @FXML
    private Label timerLabel;

    private static final Logger logger = LogManager.getLogger(TimerController.class);

    private TimerController() {
        this.timeIntervalService = TimeIntervalService.getInstance();
        this.runningTime = Calendar.getInstance();
    }

    public static TimerController getInstance() {
        if(timerController == null) {
            timerController = new TimerController();
        }
        return timerController;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        timerStartButton.setDisable(true);
        timerStopButton.setDisable(true);
        activeTaskLabel.setText("No task active...");

        timerStartButton.setOnMouseClicked((actionEventStart -> {
            runTimelines();
            timerStartButton.setDisable(true);
            timerStopButton.setDisable(false);
        }));

        timerStopButton.setOnMouseClicked((actionEventStop -> {
            stopTimelines();
            timerStartButton.setDisable(false);
            timerStopButton.setDisable(true);
        }));
    }

    /**
     * Launches a new timer for the selected task and adjusts operation button
     * @param task - task for which the new TimeInterval will be added
     */
    public void startNewTimer(Task task){
        // Stops current timer if one is running
        if(timerRunning)
            stopTimelines();

        this.activeTask = task;

        logger.debug("New running clock, active task is :\n {}", this.activeTask);

        activeTaskLabel.setText(this.activeTask.getTitle());

        runTimelines();

        timerStartButton.setDisable(true);
        timerStopButton.setDisable(false);
    }

    /**
     * Starts the timelines for the clock and updating TimeInterval
     */
    public void runTimelines() {
        resetTimer();
        currentTimeInterval = initTimeInterval(activeTask);
        parentController.taskViewController.updateListViewForTimer(activeTask);

        // Starts and updates the visual clock every second
        timelineClock = new Timeline(new KeyFrame(Duration.seconds(1), ev -> {
            runningTime.add(Calendar.SECOND, 1);
            timerLabel.setText(this.timerFormat.format(runningTime.getTime()));

            // keeps track of current second amount per minute, required to ensure no seconds are lost if timer
            // is stopped between each minute interval
            if(currentSeconds == 59)
                currentSeconds = -1;
            currentSeconds++;
        }));
        timelineClock.setCycleCount(Animation.INDEFINITE);

        // The current TimeInterval will be updated on the database every minute
        timelineIntervalSave = new Timeline(new KeyFrame(Duration.seconds(60), ev -> {
            currentTimeInterval.setEndTime(LocalTime.now().withNano(0));
            currentTimeInterval.setEffectiveSeconds(currentTimeInterval.getEffectiveSeconds() + 60);
            timeIntervalService.update(currentTimeInterval);
            parentController.taskViewController.updateListViewForTimer(activeTask);
            logger.debug("Time interval updated for task {}\n{}", activeTask.getTitle(), currentTimeInterval);
        }));
        timelineIntervalSave.setCycleCount(Animation.INDEFINITE);

        timelineClock.play();
        timelineIntervalSave.play();
        timerRunning = true;

        logger.info("Timer started");
    }

    /**
     * Stops the timelines for the clock and updating TimeInterval
     * Saves updates current TimeInterval to the database
     */
    public void stopTimelines() {
        timelineClock.stop();
        timelineIntervalSave.stop();
        logger.info("Timer stopped");

        currentTimeInterval.setEndTime(LocalTime.now().withNano(0));
        currentTimeInterval.setEffectiveSeconds(currentTimeInterval.getEffectiveSeconds() + currentSeconds);
        timeIntervalService.update(currentTimeInterval);
        parentController.taskViewController.updateListViewForTimer(activeTask);
        logger.debug("Current time interval updated for task {}, total time was : {}", activeTask, currentTimeInterval.getEffectiveSeconds());

        currentSeconds = 0;
        timerRunning = false;
    }

    /**
     * Resets the clock to "00:00:00"
     */
    public void resetTimer() {
        runningTime.set(Calendar.HOUR_OF_DAY, 0);
        runningTime.set(Calendar.MINUTE, 0);
        runningTime.set(Calendar.SECOND, 0);
    }

    /**
     * Initialises a new TimeInterval for the timer operations and adds it to the database
     * @param task - Task for which the new TimeInterval will be added
     * @return - returns a new instance of TimeInterval with initialised values
     */
    public TimeInterval initTimeInterval(Task task) {
        TimeInterval timeInterval = new TimeInterval(task);
        timeIntervalService.save(timeInterval);
        task.getTimeIntervalList().add(timeInterval);
        return timeInterval;
    }

    public void setParentController(MainWindowController parentController) {
        this.parentController = parentController;
    }

    public Label getActiveTaskLabel() {
        return activeTaskLabel;
    }

    public void setActiveTaskLabel(Label activeTaskLabel) {
        this.activeTaskLabel = activeTaskLabel;
    }
}
