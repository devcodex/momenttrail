package org.momenttrail.controller.popup;

import javafx.fxml.Initializable;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.controller.ContentControlController;
import org.momenttrail.dao.service.ProjectService;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.dto.MainElementDTO;
import org.momenttrail.model.Project;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * This class creates a new window to permit the creation or modification of a Project
 * Whether the operation is Create or Modify is specified with the usage of Enum OperationType
 *
 * @see org.momenttrail.enumeration.OperationType
 */
public class ProjectOperationsController extends AbstractOperationsController implements Initializable {

    private final ProjectService projectService;
    private Project project;

    private static final Logger logger = LogManager.getLogger(ProjectOperationsController.class);

    public ProjectOperationsController(Project project, OperationType operationType, ContentControlController parentController) {
        super(operationType, parentController);
        this.project = project;
        this.projectService = ProjectService.getInstance();
        this.setTargetObjectData(new MainElementDTO(this.project.getTitle(), this.project.getDescription()));
    }

    /**
     * @see AbstractOperationsController#checkExists()
     */
    @Override
    public void checkExists() {
        // Verifies a project already exists with the same title
        Long count = projectService.checkExists(this.getTitleTextField().getText());
        if(count > 0 && !this.getTitleTextField().getText().equals(project.getTitle())) {
            String alertTitle = "Duplicate entry";
            String alertMessage = "A project with the same title already exists!\n" +
                    "Please insert a different title.";
            launchInputError(alertTitle, alertMessage);
        }
    }

    /**
     * @see AbstractOperationsController#setOperationButtonAction()
     */
    @Override
    public void setOperationButtonAction() {
        // Updates the project object in case of a modification, and creates a new one in case of creation
        this.getOperationButton().setOnAction((actionEvent -> {
            project.setTitle(this.getTitleTextField().getText());
            project.setDescription(this.getDescriptionTextArea().getText());
            if(this.getOperationType() == OperationType.MODIFY) {
                // Updates project
                projectService.update(project);
                logger.info("Project data successfully updated.");
            } else {
                // Creates new project
                project.setAddedDate(LocalDateTime.now());
                project.setCategoryList(new ArrayList<>());
                projectService.save(project);
                logger.info("New project successfully added.");
            }
            this.getParentController().setSelectedProject(project);
            this.getParentController().refreshProjects();
            this.getParentController().getParentController().loadProjectContent(project);
            Stage stage = (Stage) this.getOperationButton().getScene().getWindow();
            stage.close();
        }));
    }
}
