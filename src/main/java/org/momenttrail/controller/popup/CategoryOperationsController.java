package org.momenttrail.controller.popup;

import javafx.fxml.Initializable;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.controller.ContentControlController;
import org.momenttrail.dao.service.CategoryService;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Category;
import org.momenttrail.model.dto.MainElementDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * This class creates a new window to permit the creation or modification of a Category
 * Whether the operation is Create or Modify is specified with the usage of Enum OperationType
 *
 * @see OperationType
 */
public class CategoryOperationsController extends AbstractOperationsController implements Initializable {

    private final CategoryService categoryService;
    private Category category;

    private static final Logger logger = LogManager.getLogger(CategoryOperationsController.class);

    public CategoryOperationsController(Category category, OperationType operationType, ContentControlController parentController) {
        super(operationType, parentController);
        this.category = category;
        this.categoryService = CategoryService.getInstance();
        this.setTargetObjectData(new MainElementDTO(this.category.getTitle(), this.category.getDescription()));
    }

    /**
     * @see AbstractOperationsController#checkExists()
     */
    @Override
    public void checkExists() {
        // Verifies if the title provided already exists for the selected project
        Long count = categoryService.checkExistsForProject(this.getTitleTextField().getText(), category.getProject());
        if(count > 0 && !this.getTitleTextField().getText().equals(category.getTitle())) {
            String alertTitle = "Category already exists";
            String alertMessage = "A category with the same title already exists in this project!\n" +
                    "Please insert a different title.";
            launchInputError(alertTitle, alertMessage);
        }
    }

    /**
     * @see AbstractOperationsController#setOperationButtonAction()
     */
    @Override
    public void setOperationButtonAction() {
        // Updates the category object in case of a modification, and creates a new one in case of creation
        this.getOperationButton().setOnAction((actionEvent -> {
            category.setTitle(this.getTitleTextField().getText());
            category.setDescription(this.getDescriptionTextArea().getText());

            if(this.getOperationType() == OperationType.MODIFY) {
                // Updates the category
                categoryService.update(category);
                logger.info("Category data successfully updated. {}", category);
                this.getParentController().getParentController().loadCategoryContent(category);
            } else {
                // Creates new category
                category.setTaskList(new ArrayList<>());
                category.setAddedDate(LocalDateTime.now());
                category.getProject().getCategoryList().add(category);
                categoryService.save(category);
                logger.info("New category successfully added. Category : {}", category);
                this.getParentController().getParentController().loadProjectContent(category.getProject());
            }
            this.getParentController().setSelectedCategory(category);
            this.getParentController().refreshCategories();
            Stage stage = (Stage) this.getOperationButton().getScene().getWindow();
            stage.close();
        }));
    }
}
