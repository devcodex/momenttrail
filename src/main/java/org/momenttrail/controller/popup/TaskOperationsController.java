package org.momenttrail.controller.popup;

import javafx.fxml.Initializable;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.controller.ContentControlController;
import org.momenttrail.dao.service.TaskService;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.dto.MainElementDTO;
import org.momenttrail.model.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * This class creates a new window to permit the creation or modification of a Task
 * Whether the operation is Create or Modify is specified with the usage of Enum OperationType
 *
 * @see OperationType
 */
public class TaskOperationsController extends AbstractOperationsController implements Initializable {

    private final TaskService taskService;
    private Task task;

    private static final Logger logger = LogManager.getLogger(TaskOperationsController.class);

    public TaskOperationsController(Task task, OperationType operationType, ContentControlController parentController) {
        super(operationType, parentController);
        this.task = task;
        this.taskService = TaskService.getInstance();
        this.setTargetObjectData(new MainElementDTO(this.task.getTitle(), this.task.getDescription()));
    }

    /**
     * @see AbstractOperationsController#checkExists()
     */
    @Override
    public void checkExists() {
        // Verifies if the title provided already exists for the selected category
        Long count = taskService.checkExistsForCategory(this.getTitleTextField().getText(), task.getCategory());
        if(count > 0 && !this.getTitleTextField().getText().equals(task.getTitle())) {
            String alertTitle = "Task already exists";
            String alertMessage = "A task with the same title already exists in this category!\n" +
                    "Please insert a different title.";
            launchInputError(alertTitle, alertMessage);
        }
    }

    /**
     * @see AbstractOperationsController#setOperationButtonAction()
     */
    @Override
    public void setOperationButtonAction() {
        // Updates the task object in case of a modification, and creates a new one in case of creation
        this.getOperationButton().setOnAction((actionEvent -> {
            task.setTitle(this.getTitleTextField().getText());
            task.setDescription(this.getDescriptionTextArea().getText());
            if(this.getOperationType() == OperationType.MODIFY) {
                taskService.update(task);
                logger.info("Task data successfully updated.");
                this.getParentController().getParentController().loadTaskContent(task);
            } else {
                task.setTimeIntervalList(new ArrayList<>());
                task.setAddedDate(LocalDateTime.now());
                task.getCategory().getTaskList().add(task);
                taskService.save(task);
                logger.info("New task successfully added.");
                this.getParentController().getParentController().loadCategoryContent(task.getCategory());
            }
            this.getParentController().setSelectedTask(task);
            this.getParentController().refreshTasks();
            Stage stage = (Stage) this.getOperationButton().getScene().getWindow();
            stage.close();
        }));
    }
}
