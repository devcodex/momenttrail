package org.momenttrail.controller.popup;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import org.momenttrail.dao.service.TimeIntervalService;
import org.momenttrail.model.TimeInterval;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class ExportController implements Initializable {
    private TimeIntervalService timeIntervalService;

    @FXML
    private DatePicker startDatePicker;
    @FXML
    private DatePicker endDatePicker;
    @FXML
    private Button exportButton;

    public ExportController() {
        timeIntervalService = TimeIntervalService.getInstance();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        exportButton.setOnMouseClicked((actionEvent -> {
            // Checks if the inserted dates are valid
            try {
                if ((endDatePicker.getValue().isBefore(startDatePicker.getValue())) || startDatePicker == null || endDatePicker == null) {
                    showErrorDialog();
                } else {
                    exportDataToFile(startDatePicker.getValue(), endDatePicker.getValue());
                }
            } catch (NullPointerException e) {
                showErrorDialog();
            }
        }));
    }

    /**
     * Retrieves all TimeIntervals from database based on provided dates and saves them in a csv file
     * @param startDate - export beginning date
     * @param endDate - export ending date
     */
    public void exportDataToFile(LocalDate startDate, LocalDate endDate) {

        List<TimeInterval> result = timeIntervalService.findAllByDateSet(startDate, endDate);

        // Opens a file writer and writes found data to file
        try(BufferedWriter writer = new BufferedWriter(
                new FileWriter(System.getProperty("user.dir") + "/exports/mtExport" + startDate.toString() + "_" + endDate.toString() + ".csv", true)
        )) {
            writer.append("project;category;task;start_date;start_time;end_time\n");
            for (TimeInterval each : result) {
                writer.append(each.getTask().getCategory().getProject().getTitle()).append(";");
                writer.append(each.getTask().getCategory().getTitle()).append(";");
                writer.append(each.getTask().getTitle()).append(";");
                writer.append(each.getStartDate().toString()).append(";");
                writer.append(each.getStartTime().toString()).append(";");
                writer.append(each.getEndTime().toString()).append("\n");
            }

            // Information window is shown upon success with the location of the export file
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Export Successful");
            alert.setHeaderText(startDatePicker.getValue() + " to " + endDatePicker.getValue());
            alert.setContentText("Your export has been saved to:\n" +
                    System.getProperty("user.dir") + "/exports/mtExport" + startDate.toString() + "_" + endDate.toString() + ".csv");

            alert.showAndWait();
            Stage stage = (Stage) this.exportButton.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            // If an error occurs an error alert is launched
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("An error occurred when trying to export data to a csv file.\n" +
                    "Ensure you have enough privileges to do so.");
        }
    }

    public void showErrorDialog(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText("The selected dates are invalid.");
        alert.showAndWait();
    }
}
