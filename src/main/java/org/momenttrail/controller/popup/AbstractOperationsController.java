package org.momenttrail.controller.popup;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.momenttrail.controller.ContentControlController;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.dto.MainElementDTO;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class AbstractOperationsController implements Initializable {
    private OperationType operationType;
    private ContentControlController parentController;
    private MainElementDTO targetObjectData;

    @FXML
    private AnchorPane contentPane;
    @FXML
    private Label titleLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextArea descriptionTextArea;
    @FXML
    private Button cancelButton;
    @FXML
    private Button operationButton;

    protected AbstractOperationsController(OperationType operationType, ContentControlController parentController) {
        this.operationType = operationType;
        this.parentController = parentController;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //This if is required, else the text field text is set as null and verifications will not work
        if(targetObjectData.getTitle() != null) {
            this.setTitleTextField(targetObjectData.getTitle());
        } else {
            this.setTitleTextField("");
        }
        this.setDescriptionTextArea(targetObjectData.getDescription());
        this.getDescriptionTextArea().setWrapText(true);

        if(this.getOperationType() == OperationType.MODIFY) {
            this.getOperationButton().setText("Modify");
        } else {
            this.getOperationButton().setText("Add");
        }

        initButtons();
    }

    public void initButtons() {
        // Attaches an event to the text field to verify if the name doesn't already exist
        // if it is a duplicate or empty an error is produced that prevents modification
        this.getTitleTextField().focusedProperty().addListener(
                (obs, oldVal, newVal) -> {
                    //If newVal is true the text field was focused, submit button is then enabled
                    if(Boolean.TRUE.equals(newVal)) {
                        this.getOperationButton().setDisable(false);
                        return;
                    }

                    if(this.getTitleTextField().getText().equals(targetObjectData.getTitle())) {
                        this.getOperationButton().setDisable(false);
                        return;
                    }

                    if(this.getTitleTextField().getText().isEmpty()) {
                        String alertTitle = "Title cannot be empty";
                        String alertMessage = "Please input a value for title";
                        launchInputError(alertTitle, alertMessage);
                        return;
                    }

                    this.checkExists();
                }
        );

        // Adds an event to cancel button to close window on click
        this.cancelButton.setOnAction((actionEvent -> {
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();
        }));

        // Updates the task object in case of a modification, and creates a new one in case of creation
        this.setOperationButtonAction();
    }

    public void launchInputError(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.showAndWait();
        operationButton.setDisable(true);
    }

    /**
     * For the current object, checks if the title already exists if Project
     * or checks if the title already exists for the same parent in case of Category or Task
     *
     * Implemented by child
     */
    public abstract void checkExists();

    /**
     * Sets the appropriate operation button action on click depending on the current object
     *
     * Implemented by child
     */
    public abstract void setOperationButtonAction();

    /*  Getters and Setters   */

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public ContentControlController getParentController() {
        return parentController;
    }

    public void setParentController(ContentControlController parentController) {
        this.parentController = parentController;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public void setTitleLabel(String titleLabel) {
        this.titleLabel.setText(titleLabel);
    }

    public Label getDescriptionLabel() {
        return descriptionLabel;
    }

    public void setDescriptionLabel(String descriptionLabel) {
        this.descriptionLabel.setText(descriptionLabel);
    }

    public TextField getTitleTextField() {
        return titleTextField;
    }

    public void setTitleTextField(String titleTextField) {
        this.titleTextField.setText(titleTextField);
    }

    public TextArea getDescriptionTextArea() {
        return descriptionTextArea;
    }

    public void setDescriptionTextArea(String descriptionTextArea) {
        this.descriptionTextArea.setText(descriptionTextArea);
    }

    public Button getCancelButton() {
        return cancelButton;
    }

    public void setCancelButton(Button cancelButton) {
        this.cancelButton = cancelButton;
    }

    public Button getOperationButton() {
        return operationButton;
    }

    public void setOperationButton(Button operationButton) {
        this.operationButton = operationButton;
    }

    public MainElementDTO getTargetObjectData() {
        return targetObjectData;
    }

    public void setTargetObjectData(MainElementDTO targetObjectData) {
        this.targetObjectData = targetObjectData;
    }
}
