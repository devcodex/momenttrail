package org.momenttrail.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.controller.popup.TaskOperationsController;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Category;
import org.momenttrail.model.Project;
import org.momenttrail.model.Task;
import org.momenttrail.model.TimeInterval;
import org.momenttrail.model.table.TaskStatsTableRow;

import java.io.IOException;

public class TaskViewController {
    ContentControlController contentControlController;
    TimerController timerController = TimerController.getInstance();

    /* Properties */
    private Task task;

    /* UI Elements */
    @FXML
    private AnchorPane contentPane;
    @FXML
    private Label projectTitleLabel;
    @FXML
    private Label categoryTitleLabel;
    @FXML
    private Label taskTitleLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Button buttonStartTimer;
    @FXML
    private Button buttonDelete;
    @FXML
    private Button buttonModify;

    /* Table View Elements */
    @FXML
    private TableView<TaskStatsTableRow> taskStatsTable;
    @FXML
    private TableColumn<TaskStatsTableRow, String> dateColumn = new TableColumn<>();
    @FXML
    private TableColumn<TaskStatsTableRow, String> startColumn = new TableColumn<>();
    @FXML
    private TableColumn<TaskStatsTableRow, String> endColumn = new TableColumn<>();
    @FXML
    private TableColumn<TaskStatsTableRow, String> totalTimeColumn = new TableColumn<>();

    private static final Logger logger = LogManager.getLogger(TaskViewController.class);

    public TaskViewController(ContentControlController contentControlController) {
        this.contentControlController = contentControlController;
    }

    /**
     * Initialises the content of the view
     * The controller itself is initialised in MainWindowController
     * @see org.momenttrail.controller.MainWindowController
     * The contents of this view are instanciated with the following method
     * @see org.momenttrail.controller.MainWindowController#loadTaskContent(Task)
     */
    public void initializeContent() {
        Project project = task.getCategory().getProject();
        Category category = task.getCategory();

        setProjectTitleLabel(project.getTitle());
        setCategoryTitleLabel(category.getTitle());
        setTaskTitleLabel(task.getTitle());
        setDescriptionLabel(task.getDescription());

        initButtons();
        initListView();
    }

    /**
     * Loads a new window to allow the modification of a given Task
     * @param task - Task object to be modified
     */
    public void loadModifyTaskWindow(Task task) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
            fxmlLoader.setController(new TaskOperationsController(task, OperationType.MODIFY, contentControlController));
            Stage stage = new Stage();
            stage.setTitle("Modify " + task.getTitle());
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.showAndWait();
        } catch (IOException e) {
            logger.error("Failed to load fxml for task pane.");
        }
    }

    /**
     * Initialises the button actions for this view
     */
    private void initButtons() {
        buttonStartTimer.setOnMouseClicked(actionEvent -> timerController.startNewTimer(task));

        buttonDelete.setOnAction((actionEvent -> contentControlController.onTaskDeleteAction(this.task)));

        buttonModify.setOnAction((actionEvent -> loadModifyTaskWindow(this.task)));
    }

    /**
     * Initialises ListView of TimeIntervals for this controller
     */
    private void initListView() {
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateColumn.prefWidthProperty().bind(taskStatsTable.widthProperty().multiply(0.25));
        startColumn.setCellValueFactory(new PropertyValueFactory<>("start"));
        startColumn.prefWidthProperty().bind(taskStatsTable.widthProperty().multiply(0.25));
        endColumn.setCellValueFactory(new PropertyValueFactory<>("end"));
        endColumn.prefWidthProperty().bind(taskStatsTable.widthProperty().multiply(0.25));
        totalTimeColumn.setCellValueFactory(new PropertyValueFactory<>("totalTime"));
        totalTimeColumn.prefWidthProperty().bind(taskStatsTable.widthProperty().multiply(0.25));

        ObservableList<TaskStatsTableRow> taskStatsTableList = FXCollections.observableArrayList();
        for (TimeInterval timeInterval : this.task.getTimeIntervalList()) {
            TaskStatsTableRow taskStatsTableRow = new TaskStatsTableRow();
            taskStatsTableRow.date.set(timeInterval.getStartDate().toString());
            taskStatsTableRow.start.set(timeInterval.getStartTime().toString());
            taskStatsTableRow.end.set(timeInterval.getEndTime().toString());
            taskStatsTableRow.totalTime.set(convertSecondsToTime(timeInterval.getEffectiveSeconds()));
            taskStatsTableList.add(taskStatsTableRow);
        }

        taskStatsTable.setItems(taskStatsTableList);
    }

    public void updateListViewForTimer(Task task) {
        if(task == this.task) {
            initListView();
        }
    }

    public String convertSecondsToTime(int totalSecs) {
        return String.format("%02d:%02d:%02d", totalSecs / 3600, (totalSecs % 3600) / 60, totalSecs % 60);
    }

    /* Getters and Setters */
    public String getProjectTitleLabel() {
        return projectTitleLabel.getText();
    }

    public void setProjectTitleLabel(String title) {
        this.projectTitleLabel.setText(title);
    }

    public String getCategoryTitleLabel() {
        return projectTitleLabel.getText();
    }

    public void setCategoryTitleLabel(String title) {
        this.categoryTitleLabel.setText(title);
    }

    public String getDescriptionLabel() {
        return descriptionLabel.getText();
    }

    public String getTaskTitleLabel() {
        return taskTitleLabel.getText();
    }

    public void setTaskTitleLabel(String title) {
        this.taskTitleLabel.setText(title);
    }

    public void setDescriptionLabel(String descriptionLabel) {
        this.descriptionLabel.setText(descriptionLabel);
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
