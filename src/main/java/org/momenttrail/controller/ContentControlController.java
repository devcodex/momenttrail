package org.momenttrail.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.contextmenu.ListingCategoryContextMenu;
import org.momenttrail.contextmenu.ListingProjectContextMenu;
import org.momenttrail.contextmenu.ListingTaskContextMenu;
import org.momenttrail.controller.popup.ProjectOperationsController;
import org.momenttrail.dao.service.CategoryService;
import org.momenttrail.dao.service.ProjectService;
import org.momenttrail.dao.service.TaskService;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Category;
import org.momenttrail.model.Project;
import org.momenttrail.model.Task;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ContentControlController implements Initializable {

    MainWindowController parentController;

    /* Listings objects DAO services */
    private ProjectService projectService = ProjectService.getInstance();
    private CategoryService categoryService = CategoryService.getInstance();
    private TaskService taskService = TaskService.getInstance();

    /* ListViews selectable objects */
    private Project selectedProject;
    private Category selectedCategory;
    private Task selectedTask;


    /* Observable lists to contain listings objects */
    final ObservableList<Project> projectObservableList = FXCollections.observableArrayList();
    final ObservableList<Category> categoryObservableList = FXCollections.observableArrayList();
    final ObservableList<Task> taskObservableList = FXCollections.observableArrayList();


    /* FXML view elements */
    @FXML
    private AnchorPane contentPane;
    @FXML
    private Accordion listingAccordion;
    @FXML
    private TitledPane listingsAccordionTitle;
    @FXML
    private TitledPane reportsAccordionTitle;

    @FXML
    private Label projectsListTitle;
    @FXML
    private Label categoriesListTitle;
    @FXML
    private Label tasksListTitle;

    @FXML
    private Button addProjectButton;

    @FXML
    private ListView<Project> projectListView;
    @FXML
    private ListView<Category> categoryListView;
    @FXML
    private ListView<Task> taskListView;

    private static final Logger logger = LogManager.getLogger(ContentControlController.class);

    @SneakyThrows
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listingAccordion.setExpandedPane(listingsAccordionTitle);
        initLabels();
        initCellFactories();
        initButtons();
        refreshProjects();

        // Feature not currently in use
        reportsAccordionTitle.setVisible(false);
    }

    /**
     * Initialises labels
     */
    private void initLabels() {
        this.listingsAccordionTitle.setText("Listings");
        this.reportsAccordionTitle.setText("Reports");
        this.projectsListTitle.setText("Projects");
        this.categoriesListTitle.setText("Categories");
        this.tasksListTitle.setText("Tasks");
    }

    /**
     * Tears down and re/builds list of projects
     */
    public void refreshProjects() {
        projectObservableList.clear();
        logger.debug("Refreshing projects with {}", this.selectedProject);
        List<Project> projectList = projectService.findAll();
        if(!projectList.isEmpty()) {
            projectObservableList.addAll(projectList);
            projectListView.setItems(projectObservableList);
        }
    }

    /**
     * Tears down and rebuilds list of categories associated with selected Project
     */
    public void refreshCategories() {
        categoryObservableList.clear();
        logger.debug("Refreshing categories for project {}", this.selectedProject);
        List<Category> categoryList = categoryService.findAllByProject(this.selectedProject);
        if(!categoryList.isEmpty()) {
            categoryObservableList.addAll(categoryList);
            categoryListView.setItems(categoryObservableList);
        }
    }

    /**
     * Tears down and rebuilds list of tasks associated with selected category
     */
    public void refreshTasks() {
        taskObservableList.clear();
        logger.debug("Refreshing tasks for category {}", this.selectedCategory);
        List<Task> taskList = taskService.findAllByCategory(this.selectedCategory);
        if(!taskList.isEmpty()) {
            taskObservableList.addAll(taskList);
            taskListView.setItems(taskObservableList);
        }
    }

    /**
     * Initialises all the required cell factories for each ListView
     */
    public void initCellFactories() {
        initProjectCellFactory();
        initCategoryCellFactory();
        initTaskCellFactory();
    }

    /* Cell factories initialization */

    /**
     * Initialises cell factory for Project ListView and defines it's selected event
     */
    public void initProjectCellFactory() {
        projectListView.setPlaceholder(new Label ("No projects available..."));
        projectListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        projectListView.setCellFactory(params -> new ListCell<>() {
            @Override
            public void updateItem(Project project, boolean empty) {
                super.updateItem(project, empty) ;
                setText(empty ? null : project.getTitle());
                this.setContextMenu(new ListingProjectContextMenu(project, ContentControlController.this));
                ContentControlController.this.selectedProject = null;
            }
        });

        this.projectListView.setOnMouseClicked(mouseEvent -> {
            this.selectedProject = projectListView.getSelectionModel().getSelectedItem();
            logger.debug("New project selected: {}", this.selectedProject);
            refreshCategories();
            refreshTasks();
            parentController.loadProjectContent(this.selectedProject);
        });
    }

    /**
     * Initialises cell factory for Category ListView and defines it's selected event
     */
    public void initCategoryCellFactory() {
        this.categoryListView.setPlaceholder(new Label ("No categories available..."));
        this.categoryListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.categoryListView.setCellFactory(params -> new ListCell<>() {
            @Override
            public void updateItem(Category category, boolean empty) {
                super.updateItem(category, empty) ;
                setText(empty ? null : category.getTitle());
                this.setContextMenu(new ListingCategoryContextMenu(category, ContentControlController.this));
                ContentControlController.this.selectedCategory = null;
            }
        });

        this.categoryListView.setOnMouseClicked(mouseEvent -> {
            this.selectedCategory = this.categoryListView.getSelectionModel().getSelectedItem();
            logger.debug("New category selected: {}", this.selectedCategory);
            refreshTasks();
            this.parentController.loadCategoryContent(this.selectedCategory);
        });
    }

    /**
     * Initialises cell factory for Task ListView and defines it's selected event
     */
    public void initTaskCellFactory() {
        this.taskListView.setPlaceholder(new Label ("No tasks available..."));
        this.taskListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.taskListView.setCellFactory(params -> new ListCell<>() {
            @Override
            public void updateItem(Task task, boolean empty) {
                super.updateItem(task, empty) ;
                setText(empty ? null : task.getTitle());
                this.setContextMenu(new ListingTaskContextMenu(task, ContentControlController.this));
                ContentControlController.this.selectedTask = null;
            }
        });

        taskListView.setOnMouseClicked(mouseEvent -> {
            this.selectedTask = this.taskListView.getSelectionModel().getSelectedItem();
            logger.debug("New task selected: {}", this.selectedTask);
            this.parentController.loadTaskContent(this.selectedTask);
        });
    }

    /* End cell factories initialization */

    /**
     * Initialises the action associated with each button
     */
    public void initButtons() {
        this.addProjectButton.setOnAction((actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
                fxmlLoader.setController(new ProjectOperationsController(new Project(), OperationType.CREATE, ContentControlController.this));
                Stage stage = new Stage();
                stage.setTitle("Add new project");
                stage.setScene(new Scene(fxmlLoader.load()));
                stage.showAndWait();
            } catch (IOException e) {
                logger.error("Failed to load fxml to add new project.");
            }
        }));
    }

    /*                                    Delete actions                                       */

    /**
     * Launches a dialog to delete the given Project
     * @param project - Project to be deleted
     */
    public void onProjectDeleteAction(Project project) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Project Deletion");
        alert.setHeaderText("Delete project " + project.getTitle());
        alert.setContentText("Are you sure you wish to delete project " + project.getTitle() + "?\n" +
                "(This project contains " + project.getCategoryList().size() + " categories.)");

        ButtonType okButton = ButtonType.YES;
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(okButton, noButton);
        alert.showAndWait().ifPresent(answer -> {
            if(answer == ButtonType.YES) {
                projectService.delete(project);
                this.selectedProject = null;
                this.selectedCategory = null;
                this.selectedTask = null;
                logger.info(String.format("Project %s was deleted.", project.getTitle()));
                refreshProjects();
                refreshCategories();
                refreshTasks();
                parentController.loadProjectContent(null);
            }
        });
    }

    /**
     * Launches a dialog to delete the given Category
     * @param category - Category to be deleted
     */
    public void onCategoryDeleteAction(Category category) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Category Deletion");
        alert.setHeaderText("Delete category " + category.getTitle());
        alert.setContentText("Are you sure you wish to delete category + " + category.getTitle() + "?\n" +
                "(This category contains " + category.getTaskList().size() + " tasks.)");

        ButtonType okButton = ButtonType.YES;
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(okButton, noButton);
        alert.showAndWait().ifPresent(answer -> {
            if(answer == ButtonType.YES) {
                category.getProject().getCategoryList().remove(category);
                categoryService.delete(category);
                this.selectedCategory = null;
                this.selectedTask = null;
                logger.info(String.format("Category %s was deleted.", category.getTitle()));
                refreshCategories();
                refreshTasks();
                parentController.loadCategoryContent(null);
            }
        });
    }

    /**
     * Launches a dialog to delete the given Task
     * @param task - Task to be deleted
     */
    public void onTaskDeleteAction(Task task) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Task Deletion");
        alert.setHeaderText("Delete task " + task.getTitle());
        alert.setContentText("Are you sure you wish to delete task " + task.getTitle() + "?\n" +
                "(This task contains " + task.getTimeIntervalList().size() + " time records.)");

        ButtonType okButton = ButtonType.YES;
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(okButton, noButton);
        alert.showAndWait().ifPresent(answer -> {
            if(answer == ButtonType.YES) {
                task.getCategory().getTaskList().remove(task);
                taskService.delete(task);
                this.selectedTask = null;
                logger.info(String.format("Task %s was deleted.", task.getTitle()));
                refreshTasks();
                parentController.loadTaskContent(null);
            }
        });
    }

    /*                                    End Delete actions                                       */

    /*                                    Getter and Setters                                       */

    public MainWindowController getParentController() { return this.parentController; }
    public void setParentController(MainWindowController parentController) {
        this.parentController = parentController;
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public Task getSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(Task selectedTask) {
        this.selectedTask = selectedTask;
    }
}
