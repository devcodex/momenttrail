package org.momenttrail.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.controller.popup.CategoryOperationsController;
import org.momenttrail.controller.popup.TaskOperationsController;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Category;
import org.momenttrail.model.Project;
import org.momenttrail.model.Task;
import org.momenttrail.model.table.CategoryStatsTableRow;

import java.io.IOException;

/**
 * This class creates a new window to permit the creation or modification of a Category
 * Whether the operation is Create or Modify is specified with the usage of Enum OperationType
 *
 * @see OperationType
 */
public class CategoryViewController {
    MainWindowController parentController;
    ContentControlController contentControlController;

    /* Properties */
    private Category category;

    /* UI Elements */
    @FXML
    private AnchorPane contentPane;
    @FXML
    private Label projectTitleLabel;
    @FXML
    private Label categoryTitleLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Button buttonAddTask;
    @FXML
    private Button buttonDelete;
    @FXML
    private Button buttonModify;

    /* Table View Elements */
    @FXML
    private TableView<CategoryStatsTableRow> categoryStatsTable;
    @FXML
    private TableColumn<CategoryStatsTableRow, String> taskColumn = new TableColumn<>();
    @FXML
    private TableColumn<CategoryStatsTableRow, String> entriesColumn = new TableColumn<>();

    private static final Logger logger = LogManager.getLogger(CategoryViewController.class);

    /**
     * Initialises the content of the view
     * The controller itself is initialised in MainWindowController
     * @see org.momenttrail.controller.MainWindowController
     * The contents of this view are instanciated with the following method
     * @see org.momenttrail.controller.MainWindowController#loadCategoryContent(Category)
     */
    public void initializeContent() {
        Project project = category.getProject();

        setProjectTitleLabel(project.getTitle());
        setCategoryTitleLabel(category.getTitle());
        setDescriptionLabel(category.getDescription());

        initButtons();
        initListView();
    }

    /**
     * Loads a new window to allow the modification of a given Category
     * @param category - Category object to be modified
     */
    public void loadModifyCategoryWindow(Category category) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
            fxmlLoader.setController(new CategoryOperationsController(category, OperationType.MODIFY, contentControlController));
            Stage stage = new Stage();
            stage.setTitle("Modify " + category.getTitle());
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.showAndWait();
        } catch (IOException e) {
            logger.error("Failed to load fxml for category modification.");
        }
    }

    /**
     * Loads a new window to allow the addition of a new category to current project
     */
    public void loadAddTaskWindow() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
            Task task = new Task();
            task.setCategory(this.category);
            fxmlLoader.setController(new TaskOperationsController(task, OperationType.CREATE, contentControlController));
            Stage stage = new Stage();
            stage.setTitle("Add new task");
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.showAndWait();
        } catch (IOException e) {
            logger.error("Failed to load fxml for adding new task.");
        }
    }

    private void initButtons() {
        buttonAddTask.setOnAction((actionEvent -> loadAddTaskWindow()));

        buttonDelete.setOnAction((actionEvent -> contentControlController.onCategoryDeleteAction(category)));

        buttonModify.setOnAction((actionEvent -> loadModifyCategoryWindow(this.category)));
    }

    /**
     * Initialises ListView of tasks for this controller
     */
    private void initListView() {
        taskColumn.setCellValueFactory(new PropertyValueFactory<>("task"));
        taskColumn.prefWidthProperty().bind(categoryStatsTable.widthProperty().multiply(0.8));
        entriesColumn.setCellValueFactory(new PropertyValueFactory<>("numEntries"));
        entriesColumn.prefWidthProperty().bind(categoryStatsTable.widthProperty().multiply(0.2));

        ObservableList<CategoryStatsTableRow> categoryStatsTableList = FXCollections.observableArrayList();
        for (Task task : this.category.getTaskList()) {
            CategoryStatsTableRow categoryStatsTableRow = new CategoryStatsTableRow();
            categoryStatsTableRow.task.set(task.getTitle());
            categoryStatsTableRow.numEntries.set(Integer.toString(task.getTimeIntervalList().size()));
            categoryStatsTableList.add(categoryStatsTableRow);
        }

        categoryStatsTable.setItems(categoryStatsTableList);
    }

    /* Getters and Setters */
    public String getProjectTitleLabel() {
        return projectTitleLabel.getText();
    }

    public void setProjectTitleLabel(String title) {
        this.projectTitleLabel.setText(title);
    }

    public String getCategoryTitleLabel() {
        return projectTitleLabel.getText();
    }

    public void setCategoryTitleLabel(String title) {
        this.categoryTitleLabel.setText(title);
    }

    public String getDescriptionLabel() {
        return descriptionLabel.getText();
    }

    public void setDescriptionLabel(String descriptionLabel) {
        this.descriptionLabel.setText(descriptionLabel);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
