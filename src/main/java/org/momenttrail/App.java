package org.momenttrail;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.momenttrail.utils.GeneralUtils;

import java.io.IOException;

public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {

        scene = new Scene(GeneralUtils.loadFXML("mainWindowV2"));
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.setOnCloseRequest((actionEvent -> {
            Platform.exit();
            System.exit(0);
        }));
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(GeneralUtils.loadFXML(fxml));
    }

    public static void main(String[] args) {
        launch();
    }

}