package org.momenttrail.contextmenu;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.controller.ContentControlController;
import org.momenttrail.controller.popup.CategoryOperationsController;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Category;

import java.io.IOException;

/**
 * This class is used to create context menus for launching Category operations directly from the Listings menu
 * Instances of this class should be injected into the cell factory of the Category list
 *
 * @see ContentControlController
 */
public class ListingCategoryContextMenu extends ContextMenu {

    MenuItem modify;
    MenuItem delete;

    private static final Logger logger = LogManager.getLogger(ListingCategoryContextMenu.class);

    /**
     * Creates instance of ContextMenu for Category listings
     * @param category - Category on which the context menu was launched
     * @param controller - Controller required to reset non-category data
     *
     * @see ContentControlController#initCategoryCellFactory()
     */
    public ListingCategoryContextMenu(Category category, ContentControlController controller) {
        super();
        this.modify = new MenuItem("Modify");
        this.delete = new MenuItem("Delete");

        // Adds opening of a new operations window to the modify context button
        this.modify.setOnAction((actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
                fxmlLoader.setController(new CategoryOperationsController(category, OperationType.MODIFY, controller));
                Stage stage = new Stage();
                stage.setTitle("Modify " + category.getTitle());
                stage.setScene(new Scene(fxmlLoader.load()));
                stage.showAndWait();
            } catch (IOException e) {
                logger.error("Failed to load fxml for category modification.");
            }
        }));

        // Adds opening category deletion dialog to the delete context button
        this.delete.setOnAction((actionEvent -> controller.onCategoryDeleteAction(category)));

        this.getItems().addAll(modify, delete);
    }
}
