package org.momenttrail.contextmenu;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.momenttrail.App;
import org.momenttrail.controller.ContentControlController;
import org.momenttrail.controller.popup.TaskOperationsController;
import org.momenttrail.enumeration.OperationType;
import org.momenttrail.model.Task;

import java.io.IOException;

/**
 * This class is used to create context menus for launching Task operations directly from the Listings menu
 * Instances of this class should be injected into the cell factory of the Task list
 *
 * @see ContentControlController
 */
public class ListingTaskContextMenu extends ContextMenu {

    MenuItem modify;
    MenuItem delete;
    MenuItem startTimer;

    private static final Logger logger = LogManager.getLogger(ListingTaskContextMenu.class);

    /**
     * Creates instance of ContextMenu for Task listings
     * @param task - Task on which the context menu was launched
     * @param controller - Controller required to reset non-category data
     *
     * @see ContentControlController#initTaskCellFactory()
     */
    public ListingTaskContextMenu(Task task, ContentControlController controller) {
        super();
        this.modify = new MenuItem("Modify...");
        this.delete = new MenuItem("Delete...");
        this.startTimer = new MenuItem("Start timer");

        // Adds opening of a new operations window to the modify context button
        this.modify.setOnAction((actionEvent -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("popup/modifyMainElement.fxml"));
                fxmlLoader.setController(new TaskOperationsController(task, OperationType.MODIFY, controller));
                Stage stage = new Stage();
                stage.setTitle("Modify " + task.getTitle());
                stage.setScene(new Scene(fxmlLoader.load()));
                stage.showAndWait();
            } catch (IOException e) {
                logger.error("Failed to load fxml for task modification.");
            }
        }));

        // Adds opening category deletion dialog to the delete context button
        this.delete.setOnAction((actionEvent -> controller.onTaskDeleteAction(task)));

        // Adds action to launch a new timer in TimerPane to the startTime context button
        this.startTimer.setOnAction((actionEvent -> logger.info("This action will serve to launch a timer for the current task (Not yet implemented)")));

        this.getItems().addAll(startTimer, modify, delete);
    }
}
