package org.momenttrail.model.table;

import javafx.beans.property.SimpleStringProperty;

public class TaskStatsTableRow {
    public final SimpleStringProperty date = new SimpleStringProperty();
    public final SimpleStringProperty start = new SimpleStringProperty();
    public final SimpleStringProperty end = new SimpleStringProperty();
    public final SimpleStringProperty totalTime = new SimpleStringProperty();

    public String getDate() { return this.date.get(); }
    public String getStart() { return this.start.get(); }
    public String getEnd() { return this.end.get(); }
    public String getTotalTime() { return this.totalTime.get(); }
}
