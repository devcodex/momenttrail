package org.momenttrail.model.table;

import javafx.beans.property.SimpleStringProperty;

public class CategoryStatsTableRow {
    public final SimpleStringProperty task = new SimpleStringProperty();
    public final SimpleStringProperty numEntries = new SimpleStringProperty();

    public String getTask() {
        return this.task.get();
    }

    public String getNumEntries() {
        return this.numEntries.get();
    }


}
