package org.momenttrail.model.table;

import javafx.beans.property.SimpleStringProperty;

public class ProjectStatsTableRow {
    public final SimpleStringProperty category = new SimpleStringProperty();
    public final SimpleStringProperty numTasks = new SimpleStringProperty();

    public String getCategory() {
        return this.category.get();
    }

    public String getNumTasks() {
        return this.numTasks.get();
    }


}
