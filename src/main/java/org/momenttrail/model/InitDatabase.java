package org.momenttrail.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.momenttrail.utils.HibernateUtil;

import java.time.LocalDateTime;

public class InitDatabase {

    private InitDatabase(){}

    public static void initProjects() {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction t = session.beginTransaction();

        Project project1 = new Project();
        project1.setTitle("Project 1");
        project1.setDescription("Description for project 1");
        project1.setAddedDate(LocalDateTime.now());

        Project project2 = new Project();
        project2.setTitle("Project 2");
        project2.setDescription("Description for project 2");
        project2.setAddedDate(LocalDateTime.now());

        Project project3 = new Project();
        project3.setTitle("Project 3");
        project3.setDescription("Description for project 3");
        project3.setAddedDate(LocalDateTime.now());

        session.save(project1);
        session.save(project2);
        session.save(project3);

        Category category1 = new Category();
        Category category2 = new Category();
        Category category3 = new Category();
        Category category4 = new Category();

        category1.setTitle("Category 1");
        category1.setProject(project1);
        category1.setAddedDate(LocalDateTime.now());

        category2.setTitle("Category 2");
        category2.setProject(project1);
        category2.setAddedDate(LocalDateTime.now());

        category3.setTitle("Category 1");
        category3.setProject(project2);
        category3.setAddedDate(LocalDateTime.now());

        category4.setTitle("Category 2");
        category4.setProject(project2);
        category4.setAddedDate(LocalDateTime.now());

        session.save(category1);
        session.save(category2);
        session.save(category3);
        session.save(category4);

        Task task1 = new Task();
        task1.setTitle("Task 1");
        task1.setAddedDate(LocalDateTime.now());
        task1.setCategory(category1);

        Task task2 = new Task();
        task2.setTitle("Task 2");
        task2.setAddedDate(LocalDateTime.now());
        task2.setCategory(category1);

        Task task3 = new Task();
        task3.setTitle("Task 1");
        task3.setAddedDate(LocalDateTime.now());
        task3.setCategory(category2);

        session.save(task1);
        session.save(task2);
        session.save(task3);

        t.commit();
        session.close();
    }
}
