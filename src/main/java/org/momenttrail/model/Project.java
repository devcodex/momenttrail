package org.momenttrail.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true, nullable = false)
    private String title;

    @Column(columnDefinition="TEXT")
    private String description;

    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime addedDate;

    @Column(nullable = false)
    private boolean archived;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Category> categoryList;

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", added_date=" + addedDate +
                ", archived=" + archived +
                '}';
    }
}
