package org.momenttrail.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String title;

    @Column
    private String description;

    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime addedDate;

    @Column(nullable = false)
    private boolean archived;

    // Binds class to Project class
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private Project project;

    // Binds class to Task class
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Task> taskList;

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", added_date=" + addedDate +
                ", archived=" + archived +
                ", project=" + project.getTitle() +
                '}';
    }
}
