package org.momenttrail.model.dto;

import java.time.LocalDate;

public class DataExportDTO {
    private String projectTitle;
    private String categoryTitle;
    private String taskTitle;
    private LocalDate beginDate;
    private String beginTime;
    private String finishTime;

    @Override
    public String toString() {
        return projectTitle + ';' + ";" + categoryTitle + ";" + taskTitle + ";" + beginDate +";" + beginTime + ";" + finishTime + "\n";
    }
}
