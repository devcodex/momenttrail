package org.momenttrail.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainElementDTO {
    private String title;
    private String description;

    public MainElementDTO(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
