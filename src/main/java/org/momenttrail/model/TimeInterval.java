package org.momenttrail.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "time_interval")
public class TimeInterval {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalTime startTime;

    @Column(nullable = false)
    private LocalTime endTime;

    @Column(nullable = false)
    private int effectiveSeconds;

    @Column(nullable = false)
    private boolean archived;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id")
    private Task task;

    public TimeInterval(Task task) {
        startDate = LocalDate.now();
        startTime = LocalTime.now().withNano(0);
        endTime = LocalTime.now().withNano(0);
        effectiveSeconds = 0;
        this.task = task;
    }


    @Override
    public String toString() {
        return "TimeInterval{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", startTime=" + startTime +
                ", endtTime=" + endTime +
                ", effectiveSeconds=" + effectiveSeconds +
                ", archived=" + archived +
                '}';
    }
}
