package org.momenttrail.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.momenttrail.App;

import java.io.IOException;
import java.math.BigInteger;

public class GeneralUtils {
    private GeneralUtils() {}

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static String convertSecondsToHours(BigInteger totalSecs) {
        if(totalSecs != null) {
            return String.format("%s", totalSecs.longValue() / 3600);
        } else {
            return "0";
        }
    }
}
