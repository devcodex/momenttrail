package org.momenttrail.enumeration;

public enum OperationType {
    CREATE,
    MODIFY,
    DELETE
}
