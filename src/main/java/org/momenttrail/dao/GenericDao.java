package org.momenttrail.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T, I extends Serializable> {

    public T findById(I id);

    public List<T> findAll();

    public void create(T entity);

    public void update(T entity);

    public void delete(T entity);
}
