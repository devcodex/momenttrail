package org.momenttrail.dao.service;

import org.hibernate.query.Query;
import org.momenttrail.dao.GenericHibernateDAO;
import org.momenttrail.model.TimeInterval;

import java.time.LocalDate;
import java.util.List;

public class TimeIntervalService extends GenericHibernateDAO<TimeInterval, String> {

    private static TimeIntervalService timeIntervalService;

    public TimeIntervalService() {
        super();
        getSession();
    }

    public static TimeIntervalService getInstance() {
        if(timeIntervalService == null) {
            timeIntervalService = new TimeIntervalService();
        }
        return timeIntervalService;
    }

    public List<TimeInterval> findAllByDateSet(LocalDate startDate, LocalDate endDate) {
        Query<TimeInterval> query = getSession().createQuery("from TimeInterval where startDate >=:startDate and startDate <= :endDate")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate);
        return query.getResultList();
    }
}
