package org.momenttrail.dao.service;

import org.hibernate.query.Query;
import org.momenttrail.dao.GenericHibernateDAO;
import org.momenttrail.model.Category;
import org.momenttrail.model.Task;

import java.math.BigInteger;
import java.util.List;

public class TaskService extends GenericHibernateDAO<Task, String> {

    private static TaskService taskService;

    private TaskService() {
        super();
        getSession();
    }

    public static TaskService getInstance() {
        if(taskService == null) {
            taskService = new TaskService();
        }
        return taskService;
    }

    public Long checkExistsForCategory(String title, Category category) {
        Query<Long> query = getSession().createQuery("select count(*) from Task where title=:title and category =:category")
                .setParameter("title", ""+title)
                .setParameter("category", category);
        return query.uniqueResult();
    }

    public List<Task> findAllByCategory(Category category) {
        Query<Task> query = getSession().createQuery("from Task where category =:category")
                .setParameter("category", category);
        return query.getResultList();
    }

    public BigInteger findLifetimeSeconds(Task task) {
        Query<BigInteger> query = getSession().createSQLQuery(
                "select sum(effectiveseconds) " +
                        "from task as t " +
                        "    left join time_interval as ti on t.id=ti.task_id " +
                        "where t.id=:project_id")
                .setParameter("task_id", task.getId());
        return query.uniqueResult();
    }
}
