package org.momenttrail.dao.service;

import org.hibernate.query.Query;
import org.momenttrail.dao.GenericHibernateDAO;
import org.momenttrail.model.Project;

public class ProjectService extends GenericHibernateDAO<Project, String> {

    private static ProjectService projectService;

    private ProjectService() {
        super();
        getSession();
    }

    public static ProjectService getInstance() {
        if(projectService == null) {
            projectService = new ProjectService();
        }
        return projectService;
    }

    public Project findByTitle(String title) {
        Query<Project> query = getSession().createQuery("from Project where title=:title")
                .setParameter("title", ""+title);
        return query.getSingleResult();
    }

    public Long checkExists(String title) {
        Query<Long> query = getSession().createQuery("select count(*) from Project where title=:title")
                .setParameter("title", ""+title);
        return query.uniqueResult();
    }
}
