package org.momenttrail.dao.service;

import org.hibernate.query.Query;
import org.momenttrail.dao.GenericHibernateDAO;
import org.momenttrail.model.Category;
import org.momenttrail.model.Project;

import java.util.List;

public class CategoryService extends GenericHibernateDAO<Category, String> {

    private static CategoryService categoryService;

    private CategoryService() {
        super();
        getSession();
    }

    public static CategoryService getInstance() {
        if(categoryService == null) {
            categoryService = new CategoryService();
        }
        return categoryService;
    }

    public Category findByTitle(String title) {
        Query<Category> query = getSession().createQuery("from Category where title=:title")
                .setParameter("title", ""+title);
        return query.getSingleResult();
    }

    public Long checkExistsForProject(String title, Project project) {
        Query<Long> query = getSession().createQuery("select count(*) from Category where title=:title and project =:project")
                .setParameter("title", ""+title)
                .setParameter("project", project);
        return query.uniqueResult();
    }

    public List<Category> findAllByProject(Project project) {
        Query<Category> query = getSession().createQuery("from Category where project =:project")
                .setParameter("project", project);
        return query.getResultList();
    }
}
