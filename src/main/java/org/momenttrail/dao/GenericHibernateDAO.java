package org.momenttrail.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.momenttrail.utils.HibernateUtil;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public class GenericHibernateDAO<T, I extends Serializable>  {

    private final Class<T> persistentClass;
    private Session session;

    public GenericHibernateDAO() {
        this.persistentClass = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void setSession(Session s) {
        this.session = s;
    }

    protected Session getSession() {
        if (session == null) {
            session = HibernateUtil.getSession();
        }
        return session;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    public T findById(int id) {
        return getSession().load(getPersistentClass(), id);
    }

    public List<T> findAll() {
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(getPersistentClass());

        Root<T> myObjectRoot = criteria.from(getPersistentClass());

        Query<T> query = session.createQuery(criteria);
        return query.getResultList();
    }

    public void save(T entity) {
        Transaction transaction = getSession().beginTransaction();
        getSession().save(entity);
        transaction.commit();
    }

    public void update(T entity) {
        Transaction transaction = getSession().beginTransaction();
        getSession().update(entity);
        transaction.commit();
    }

    public void delete(T entity) {
        Transaction transaction = getSession().beginTransaction();
        getSession().delete(entity);
        transaction.commit();
    }
}
