INSERT INTO public.project (id, addeddate, archived, description, title) VALUES (1, '2020-10-26 16:01:18.512381', false, 'Application to manage a Library', 'CityLib');
INSERT INTO public.project (id, addeddate, archived, description, title) VALUES (2, '2020-10-26 16:01:45.000723', false, 'Application to manage a chain of Pizzarias with a delivery system', 'OCPizzaria');
INSERT INTO public.project (id, addeddate, archived, description, title) VALUES (3, '2020-10-26 16:02:17.235301', false, 'Bank transaction management utility', 'MyERP');

INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (4, '2020-10-26 16:02:50.632267', false, null, 'Build', 1);
INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (5, '2020-10-26 16:02:54.821050', false, null, 'Run', 1);
INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (6, '2020-10-26 16:02:59.996393', false, null, 'Meetings', 1);
INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (7, '2020-10-26 16:03:11.806713', false, null, 'Misc', 1);
INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (8, '2020-10-26 16:03:19.131701', false, null, 'Conception', 2);
INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (9, '2020-10-26 16:03:27.826098', false, null, 'Refactoring', 3);
INSERT INTO public.category (id, addeddate, archived, description, title, project_id) VALUES (10, '2020-10-26 16:03:36.453018', false, null, 'Tests', 3);

INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (11, '2020-10-26 16:04:20.122664', false, 'Addition of new module to manage Books', 'Issue #1', 4);
INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (12, '2020-10-26 16:04:52.356132', false, 'Addition of new module to manage users', 'Issue #2', 4);
INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (13, '2020-10-26 16:05:38.150444', false, 'Addition of new module to manage renting of book copies', 'Issue #3', 4);
INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (14, '2020-10-26 16:05:48.456192', false, null, 'Daily', 6);
INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (15, '2020-10-26 16:06:26.300919', false, null, 'Code cleanup', 9);
INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (16, '2020-10-26 16:07:03.279710', false, null, 'New transaction tests', 10);
INSERT INTO public.task (id, addeddate, archived, description, title, category_id) VALUES (40, '2020-10-26 16:27:37.877109', false, null, 'Database schema', 8);

INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (43, false, 7, '20:28:09', '2020-10-26', '20:28:01', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (44, false, 15, '20:33:25', '2020-10-26', '20:33:10', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (39, false, 1, '10:15:00', '2020-10-23', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (32, false, 2, '10:15:00', '2020-10-26', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (34, false, 2, '10:15:00', '2020-10-19', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (35, false, 3, '10:15:00', '2020-10-21', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (30, false, 2, '10:15:00', '2020-10-16', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (31, false, 3, '10:15:00', '2020-10-20', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (33, false, 3, '10:15:00', '2020-10-22', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (29, false, 2, '10:15:00', '2020-10-18', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (21, false, 2, '09:44:59', '2020-10-26', '09:05:15', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (26, false, 3, '16:15:25', '2020-10-26', '14:08:12', 12);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (38, false, 2, '10:15:00', '2020-10-15', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (36, false, 1, '10:15:00', '2020-10-13', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (37, false, 2, '10:15:00', '2020-10-14', '09:45:00', 14);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (22, false, 5, '16:25:48', '2020-10-19', '11:02:12', 12);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (28, false, 1, '16:45:53', '2020-10-20', '10:20:13', 13);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (20, false, 3, '16:25:32', '2020-10-16', '10:45:24', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (19, false, 4, '16:25:27', '2020-10-15', '10:25:00', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (42, false, 5, '16:50:06', '2020-10-12', '11:25:56', 40);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (24, false, 3, '09:35:05', '2020-10-23', '09:15:06', 12);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (17, false, 2, '16:25:15', '2020-10-13', '13:45:23', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (23, false, 1, '16:25:51', '2020-10-20', '11:45:00', 12);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (27, false, 6, '16:10:46', '2020-10-22', '10:15:26', 13);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (41, false, 4, '19:04:00', '2020-10-23', '14:06:25', 40);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (18, false, 5, '16:25:21', '2020-10-14', '12:05:56', 11);
INSERT INTO public.time_interval (id, archived, effectiveseconds, endtime, startdate, starttime, task_id) VALUES (25, false, 4, '16:26:01', '2020-10-21', '10:26:34', 12);

SELECT setval('hibernate_sequence', 40, false);