[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=devcodex_momenttrail&metric=alert_status)](https://sonarcloud.io/dashboard?id=devcodex_momenttrail)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=devcodex_momenttrail&metric=coverage)](https://sonarcloud.io/dashboard?id=devcodex_momenttrail)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=devcodex_momenttrail&metric=security_rating)](https://sonarcloud.io/dashboard?id=devcodex_momenttrail)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=devcodex_momenttrail&metric=sqale_index)](https://sonarcloud.io/dashboard?id=devcodex_momenttrail)


# MomentTrail - A time tracking application

This application can be used to track time in a work environment.
This is an offline application and only requires an external database to be able to store the data added to it.
A local or remote database can be used through configuration.

The application allows for the creation and management of the following :

* Creation and management of Project elements
* Creation and management of Category elements
* Creation and management of Task elements
* Creation of time counters for each added task
* Exportation of time counters to CSV files

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Java 11 (Either Oracle's or OpenJDK)

Maven - if you don't have it installed you can install
         it by clicking [here](https://maven.apache.org/download.cgi).

Git (if you wish to clone or fork the repository)

A PostgreSQL server running on your local machine (Recommended version 9 or higher)

### Installation

1. Start by retrieving the project from the current repository, to do this, simply clone the project,
or download the project to your local machine.
 
2. To configure the database make changes to  MomentTrail/src/main/resources/hibernate.cfg.xml and change the following values to suit your needs :

    2. ```<property name="connection.username">postgres</property>```

    2. ```<property name="connection.password">postgres</property>```

    2. ```<property name="connection.url">jdbc:postgresql://localhost:5432/momenttrail</property>```

    2. On your PostGreSQL server create a new database with the name "momenttrail" if you don't already have one
    
    2. If you wish to load the application with some test data change the following element from "update" to "create"
    ```<property name="hbm2ddl.auto">update</property>```
    
    2. The schema creation will be automatically handled by the application
 
3. Open a terminal/console at the root of the project and run the following command to install all required dependencies:

    3. ```mvn clean install javafx:compile```

    3. This will compile the application and ready it for usage

## Running the application
To run the application simple run the following command : 

```mvn javafx:run```

A wiki with all the required information to make use of the application is available

[MomentTrail Usage Wiki](https://gitlab.com/devcodex/momenttrail/-/wikis/Usage-guide)

## Built With
[Maven](https://maven.apache.org/) - Dependency Management

## Authors

**Bruno Ferreira**
